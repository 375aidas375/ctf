#if defined _WIN32 
#define _WIN32_WINNT 0x0501
#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#else 
#define closesocket close
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define DEFAULT_BUFLEN 1024
#define DEFAULT_PORT 8124

void smsg(int ConnectSocket, char msg[DEFAULT_BUFLEN]);
char *recieve(int ConnectSocket, char recvbuf[DEFAULT_BUFLEN]);

void ClearWin(){
    #if defined _WIN32
        WSACleanup();
    #endif
}

int main()
{
    struct hostent *rem;    
    char *host_addr = "78.56.77.230"; // serverio IPv4 adresas
    struct in_addr addr;
    addr.s_addr = inet_addr(host_addr);
    int ConnectSocket;
    char *sendbuf = "Connecting"; // pirmas pranesimas prisijungus
    char rec[DEFAULT_BUFLEN];
    int iResult;
    char pran[DEFAULT_BUFLEN];

    #if defined _WIN32
        WSADATA wsaData;
        if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
        {
            printf("WSAStartup nepasileido su klaida\n");
            return 0;
        }
    #endif
    
    rem = gethostbyaddr((char *)&addr, 4, AF_INET);
    
    struct sockaddr_in sin;
    memset(&sin, 0, sizeof(sin));
    sin.sin_port = htons(DEFAULT_PORT);
    sin.sin_family = AF_INET;
    memcpy(&sin.sin_addr, rem->h_addr_list[0], sizeof(sin.sin_addr));

    ConnectSocket = socket(AF_INET, SOCK_STREAM, 0);
    
    if(connect(ConnectSocket, (struct sockaddr *)&sin, sizeof(sin)) < 0){
    	printf("Prisijungti nepaviko");
		closesocket(ConnectSocket);
		ClearWin();
    	return 0;
    }
    
    smsg(ConnectSocket, sendbuf);

    recieve(ConnectSocket, rec);

    printf("%s\n\n\n", rec);

    //Ciklas siusti pranesima
    do
    {
        printf("Rasykite pranesima, nenoredami testi parasykite 0: ");
        scanf("%s", pran);
        if (pran[0] != '0')
        {
            smsg(ConnectSocket, pran);
            recieve(ConnectSocket, rec);
            printf("%s\n\n", rec);
            rec[0] = '\0';
        }
    } while (pran[0] != '0');
    
    closesocket(ConnectSocket);
    ClearWin();
    return 0;
}

void smsg(int ConnectSocket, char msg[DEFAULT_BUFLEN])
{
    int iResult;
    iResult = send(ConnectSocket, msg, (int)strlen(msg), 0);
    if (iResult == 0)
    {
        printf("Isiusti nepaviko\n");
    }
    else
    {
        printf("Bitu issiusta: %d\n", iResult);
        //printf("Pranesimas issiustas: %s\n", msg);
    }
}

char *recieve(int ConnectSocket, char recvbuf[DEFAULT_BUFLEN])
{
    int iResult;
    int recvbuflen = DEFAULT_BUFLEN;

    iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);
    if (iResult > 0)
    {
        printf("Bitu gauta: %d\n", iResult);
        recvbuf[iResult] = '\0';
    }
    else if (iResult == 0)
    {
        printf("Atsijungta\n");
        return "0";
    }
    else
    {
        printf("Gauti pranesimo nepaviko\n");
        return "0";
    }
    return recvbuf;
}
