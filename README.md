# Programos naudijimas

Programa automatiškai prisijungia prie serverio

Prisijungus programa serveriui išsius pranešimą, gavus atsaka galima įrašyti žinute serveriui.

Norint nutraukti sesija pranesimo laukelije parasyti reikia: 0

## Programos paleidimas

Windowsams naudoti komanda:

```
gcc Main.c -o Main -lws2_32 && Main
```

Linux naudoti komanda:

```
gcc Main.c -o Main && ./Main
```
